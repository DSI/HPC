# ==============================================================================
#
# Copyright (c) 2019 Dell Technologies
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# ==============================================================================

#
# See the slurm.conf man page for more information.

#
# Where slurmctld is running
ClusterName=cluster
ControlMachine=gemini
ControlAddr=10.3.0.254

#
# Accounting
AccountingStorageType=accounting_storage/slurmdbd
AccountingStorageHost=gemini
AccountingStorageUser=slurm
# Uncomment only if you will declare user in slurmdb
AccountingStorageEnforce=qos,limits
AccountingStoreJobComment=YES

#
# Slurm User and Ports
SlurmUser=slurm
SlurmctldPort=6817
SlurmdPort=6818

#
# Authentification and Security
AuthType=auth/munge
UsePAM=1

#
# State Preservation
StateSaveLocation=/var/spool/slurm/ctld
SlurmdSpoolDir=/var/spool/slurm/d

#
# Logging
SlurmctldDebug=3
SlurmctldLogFile=/var/log/slurm/slurmctld.log
SlurmctldPidFile=/var/run/slurmctld.pid
SlurmdDebug=3
SlurmdLogFile=/var/log/slurm/slurmd.log
SlurmdPidFile=/var/run/slurmd.pid

#
# Control when a DOWN node will be returned to service
ReturnToService=2

#
# Network topology
TopologyPlugin=topology/tree
SwitchType=switch/none
MpiDefault=none

#
# Timers
SlurmctldTimeout=300
SlurmdTimeout=300
InactiveLimit=0
MinJobAge=300
KillWait=30
Waittime=0

#
# Epilog and Prolog
Prolog=/etc/slurm/slurm.prolog.mount
Epilog=/etc/slurm/slurm.epilog.clean

#
# Resource management and process tracking
ProctrackType=proctrack/cgroup
TaskPlugin=task/affinity,task/cgroup

#
# Job Accounting mechanism
JobAcctGatherType=jobacct_gather/none

#
# Consumable Resources
SelectType=select/cons_res
SelectTypeParameters=CR_Core
PropagateResourceLimitsExcept=MEMLOCK
DefMemPerCPU=1024

#
# Fast scheduling policy
#  - if set to 1, slurmctld will drain nodes if they have less
#    resources than specificied in slurmd-nodes.conf
FastSchedule=1

#
# Scheduling policy
SchedulerType=sched/backfill

#
# Partition behaviour
EnforcePartLimits=ALL

#
# Fairshare scheduling policy
# Contribution of historical usage:
#  - PriorityDecayHalfLife
#  - PriorityUsageResetPeriod
# PriorityFavorSmall:
#  - NO:  The larger the job, the greater its job size priority.
#  - YES: The smaller the job, the greater its job size priority.
PriorityType=priority/multifactor
PriorityWeightQOS=1
PrologFlags=X11,Alloc

#
# Preemption
PreemptType=preempt/partition_prio
PreemptMode=requeue

# Specify the number of stuff users can submit
# The total number of active jobs should not be
# too big or slurm performances my suffer.
MaxArraySize=20001
MaxJobCount=100000

# Move FirstJobId after slurmDB re-import
# last job was: 18069313|arafalimana|0
FirstJobId=18100000

#
# Generic Resource (GRES) Scheduling
#GresTypes=gpu,mic

#
# Default Compute nodes definition
#  - Please use option 'MemSpecLimit=4096' to keep memory for the OS
NodeName=DEFAULT State=UNKNOWN
NodeName=p[001-026]  CPUs=20 Boards=1 SocketsPerBoard=2 CoresPerSocket=10 ThreadsPerCore=1 RealMemory=64238   State=UNKNOWN Feature=IvyBridge,AVX
#NodeName=p027 State=DOWN Reason="poweron failed"
NodeName=p[028-043]  CPUs=20 Boards=1 SocketsPerBoard=2 CoresPerSocket=10 ThreadsPerCore=1 RealMemory=64238   State=UNKNOWN Feature=IvyBridge,AVX
#NodeName=p044 State=DOWN Reason="poweron failed"
NodeName=p[045-049]  CPUs=20 Boards=1 SocketsPerBoard=2 CoresPerSocket=10 ThreadsPerCore=1 RealMemory=64238   State=UNKNOWN Feature=IvyBridge,AVX
#NodeName=p050 State=DOWN Reason="bios looping"
NodeName=p[051-074]  CPUs=20 Boards=1 SocketsPerBoard=2 CoresPerSocket=10 ThreadsPerCore=1 RealMemory=64238   State=UNKNOWN Feature=IvyBridge,AVX
#NodeName=p075 State=DOWN Reason="bios looping"
NodeName=p[076-102]  CPUs=20 Boards=1 SocketsPerBoard=2 CoresPerSocket=10 ThreadsPerCore=1 RealMemory=64238   State=UNKNOWN Feature=IvyBridge,AVX
NodeName=r428        CPUs=32 Boards=1 SocketsPerBoard=4 CoresPerSocket=8  ThreadsPerCore=1 RealMemory=1031848 State=UNKNOWN Feature=SandyBridge,AVX
NodeName=suv001      CPUs=40 Boards=1 SocketsPerBoard=2 CoresPerSocket=20 ThreadsPerCore=1 RealMemory=257211  State=UNKNOWN Feature=CascadeLake,AVX,AVX2,AVX512
NodeName=v001        CPUs=40 Boards=1 SocketsPerBoard=2 CoresPerSocket=20 ThreadsPerCore=1 RealMemory=385385  State=UNKNOWN Feature=Visu
NodeName=x[001-040]  CPUs=40 Boards=1 SocketsPerBoard=2 CoresPerSocket=20 ThreadsPerCore=1 RealMemory=191737  State=UNKNOWN Feature=Skylake,AVX,AVX2,AVX512
NodeName=xa0[01-04]  CPUs=128 Boards=1 SocketsPerBoard=8 CoresPerSocket=16 ThreadsPerCore=1 RealMemory=257364 State=UNKNOWN Feature=EPYC,AVX

#
# Partition definition
#PartitionName=defq Nodes=ALL State=UP Default=YES

# Rules:
# The besteffort partition has a priority of 2 (arbitrary choice)
# You have to set a priority to 1 to forbid preemption
# You have to set a priority higher to 2 to allow preemption

# Sequential partition
PartitionName=seq       Nodes=p[031-043,045-049,051-074,076-102]                     OverSubscribe=NO        MaxTime=24:00:00 MaxNodes=1 Default=NO AllowAccounts=ALL DefaultTime=02:00:00 Priority=2 PreemptMode=OFF QOS=qosoca-seq
PartitionName=seq-short Nodes=p[001-026,028-043,045-049,051-074,076-102]             OverSubscribe=NO        MaxTime=4:00:00  MaxNodes=1 Default=NO AllowAccounts=ALL DefaultTime=02:00:00 Priority=3 PreemptMode=OFF QOS=qosoca-seqshort
PartitionName=gitlab    Nodes=p[001-026,028-043,045-049,051-074,076-102],suv001 OverSubscribe=EXCLUSIVE MaxTime=00:15:00            Default=NO AllowAccounts=ALL DefaultTime=00:15:00 Priority=1 PreemptMode=OFF QOS=qosoca-git
PartitionName=short     Nodes=p[001-026,028-043,045-049,051-074,076-102],x[001-040]  OverSubscribe=NO        MaxTime=2:00:00             Default=NO AllowAccounts=ALL DefaultTime=00:30:00 Priority=3 PreemptMode=OFF


# Parallel partition
PartitionName=fdr  Nodes=p[001-026,028-043,045-049,051-074,076-102] OverSubscribe=EXCLUSIVE MaxTime=24:00:00 Default=NO AllowAccounts=ALL DefaultTime=02:00:00 Priority=3 PreemptMode=OFF QOS=qosoca-par
PartitionName=x40  Nodes=x[001-040]                                 OverSubscribe=EXCLUSIVE MaxTime=24:00:00 Default=NO AllowAccounts=ALL DefaultTime=02:00:00 Priority=2 PreemptMode=OFF QOS=qosoca-par
PartitionName=bash Nodes=x[001-040],v001                            OverSubscribe=FORCE:2   MaxTime=8:00:00  Default=NO AllowAccounts=ALL DefaultTime=02:00:00 Priority=5 PreemptMode=OFF QOS=qosoca-bash

# Bigmem partition
PartitionName=1to Nodes=r428 OverSubscribe=NO MaxTime=24:00:00 Default=NO AllowAccounts=ALL DefaultTime=02:00:00 Priority=3 PreemptMode=OFF

# SUV partition
PartitionName=suv Nodes=suv001 OverSubscribe=NO MaxTime=24:00:00 Default=NO AllowAccounts=ALL DefaultTime=02:00:00 Priority=3 PreemptMode=OFF

# Visu partition
PartitionName=visu Nodes=v001 OverSubscribe=NO Default=NO AllowAccounts=ALL DefaultTime=02:00:00 MaxTime=24:00:00 PreemptMode=OFF

# Best-effort partition
PartitionName=besteffort Nodes=p[031-043,045-049,051-074,076-102],x[001-040],suv001 OverSubscribe=NO MaxTime=24:00:00 Default=NO AllowAccounts=ALL DefaultTime=02:00:00 Priority=2 PreemptMode=REQUEUE

# AMD partition
PartitionName=amd Nodes=xa[001-004] OverSubscribe=EXCLUSIVE MaxTime=24:00:00 Default=NO AllowAccounts=ALL DefaultTime=02:00:00 Priority=2 PreemptMode=OFF QOS=qosoca-par

### END OF FILE ###
