#!/bin/bash
# vim: set ts=4 sw=4 expandtab:
# ==============================================================================
#
# Copyright (c) 2018 Dell Technologies
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# ==============================================================================
#
# description : Ensure remote fs are mounted
# author      : Cedric Castagnede
# company     : DellEMC
# mail        : cedric.castagnede@dell.com
#
# ==============================================================================

# Define NFS mount(s)
src=($(grep _netdev /etc/fstab|awk '{print $1}'))
dst=($(grep _netdev /etc/fstab|awk '{print $2}'))

# Check if NFS mount(s) is active
count="${#src[@]}"
for (( i=0; i<$count; i++ )); do
    if ! grep -q "${src[$i]}" /proc/mounts; then
        logger "job_${SLURM_JOB_ID}: Mount ${dst[$i]}"
        mount ${dst[$i]}
    fi
done

# Check if BeeGFS client is present
if [[ -f /etc/beegfs/beegfs-mounts.conf ]];then
    # Define BeeGFS mount
    src=($(cat /etc/beegfs/beegfs-mounts.conf|awk '{print $1}'))

    # Check if BeeGFS mount is active
    count="${#src[@]}"
    for (( i=0; i<$count; i++ )); do
        if ! grep -q "${src[$i]}" /proc/mounts; then
            logger "job_${SLURM_JOB_ID}: Mount ${dst[$i]}"
            systemctl restart beegfs-client
        fi
    done
fi

# Slurm prolog must exit 0
exit 0

