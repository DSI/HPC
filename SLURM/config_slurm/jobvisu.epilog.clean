#!/bin/bash
# ==============================================================================
#
# Copyright (c) 2018 Dell Technologies
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# ==============================================================================
#
# description     : This script is a Slurm EPILOG that stops TurboVNC server
# author          : Cedric Castagnede
# company         : DellEMC
# mail            : cedric.castagnede@dell.com
#
# ==============================================================================

# Define Slurmd logging file
SlurmdLogFile=$(scontrol show config | grep SlurmdLogFile|awk '{print $3}')

# Ensure we are in a job
if [[ -n ${SLURM_JOB_ID} ]];then
    # Collect user information
    VISU_USER=${SLURM_JOB_USER}
    VISU_HOME=$(eval echo ~${VISU_USER})
    VISU_OUTF=$(scontrol show job ${SLURM_JOB_ID}|tr ' ' '\n'|grep StdOut|cut -d= -f2)

    # Define internal variables
    TVNC_DIR="/opt/TurboVNC/bin"
    SUDO_USER="sudo -u ${VISU_USER}"


    # Perform action it output is available 
    if [[ -f ${VISU_OUTF} ]];then
        # Look for VNC port
        VISU_ID=$(grep "^vnc_host:" ${VISU_OUTF}|awk '{print $2}'|cut -d: -f2)
        echo "[jobvisu.epilog.clean][${SLURM_JOB_ID}] user=${VISU_USER}; vnc=${VISU_ID}" >> ${SlurmdLogFile}

        # Check if vncserver is still running 
        IS_VNC=$(${SUDO_USER} ${TVNC_DIR}/vncserver -list 2>/dev/null|grep ":${VISU_ID}")

        # Clean VNC server
        if [[ -n ${VISU_ID} && ${IS_VNC} ]];then
            ${SUDO_USER} ${TVNC_DIR}/vncserver -kill :${VISU_ID} > /dev/null 2>&1
        fi

        # Clean other files
        for lock in /tmp/.X${VISU_ID}-lock /tmp/.X11-unix/X${VISU_ID};do
            if [[ -f ${lock} ]];then
                ${SUDO_USER} rm -f ${lock}
            fi
        done
    fi
fi

# Success exit
exit 0
