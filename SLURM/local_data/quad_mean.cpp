#include <iostream>
#include <cmath>
#include <algorithm>
#include <numeric>
#include <vector>
#include <iterator>

int
main() {
  std::vector<float> data;
  std::copy(std::istream_iterator<float>(std::cin), std::istream_iterator<float>(),
            std::back_inserter(data));
  float qmean = std::sqrt(std::accumulate(data.begin(), data.end(), 0,
                                          [](float f1, float f2) { return f1+std::pow(f2,2); })
                          /data.size());
  std::cout << "Quad. mean of ";
  std::copy(data.begin(), data.end(), std::ostream_iterator<float>(std::cout, " "));
  std::cout << "\nis: " << qmean << '\n';
  return 0;
}
