#!/usr/bin/env bash

module purge
module load userspace/OCA
module load gnurt/8.3
module load intel/compilers/2019

echo -n building quad_mean
icpc -std=c++11 ./quad_mean.cpp -o quad_mean
echo ..done
echo -n building gen_input
icpc -std=c++11 ./gen_input.cpp -o gen_input
echo ..done

rm -rf inputs
mkdir inputs
./gen_input 100 inputs

