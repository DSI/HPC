#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <algorithm>
#include <numeric>
#include <vector>
#include <iterator>
#include <string>
#include <random>

std::string
ifname(std::string dir, int idx) {
  std::ostringstream fmt;
  fmt << dir << "/data_" << idx;
  return fmt.str();
}

int
main(int argc, char* argv[]) {
  int         nb_files = argc > 1 ? std::stoi(argv[1]) : 1;
  std::string data_dir = argc > 2 ? std::string(argv[2]) : std::string(".");
  
  // setup random number generation:
  std::random_device seed; 
  std::mt19937 gen(seed());
  std::uniform_real_distribution<float> dis(0, 84);

  std::cout << "will generate " << nb_files << " input files in " << data_dir << ".\n";
  for(int i = 0; i < nb_files; ++i) {
    std::string input = ifname(data_dir, i);
    std::cout << "generating " << input << std::flush;
    std::ofstream dfile(input);
    for(int j = 0; j < 1000; ++j) {
      dfile << dis(gen) << '\n';
    }
    std::cout << " done\n";
  }
  return 0;
}
