#!/usr/bin/env bash

# Initialize the modules
module purge

module load cmake
module load intel-gnu8-runtime/19.1.2.254

# Create the build directory
here=$(pwd $(dirname $0))
mkdir -p rel
pushd rel

# Configure and build
cmake $here || exit -1
make || exit -1
popd

# Get the command back
cp -v rel/coin ./


