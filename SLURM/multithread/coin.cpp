#include <iostream>
#include <random>
#include <functional>
#include <omp.h>

int
main(int argc, char* argv[]) {
  int nthreads;
#pragma omp parallel
#pragma omp single
  {
    nthreads = omp_get_num_threads();
  }
  std::cout << "Hi world, we are " << nthreads << " threads trying to get lucky together.\n";
  
  int const head = 0;
  int const tail = 1;
  std::default_random_engine generator;
  std::uniform_int_distribution<int> distribution(head,tail);
  auto coin = std::bind ( distribution, generator );
  int total;
  int tentative = 0;
  do {
    total = 0;
    ++tentative;
#pragma omp parallel reduction(+:total)
    {
      int result = coin();
      if (result == head) {
        ++total;
        std::cout << "head ";
      } else {
        std::cout << "tail ";
      }
    }
    std::cout << '\n';
  } while (total != nthreads);
  std::cout << "It took us " << tentative << " tries to all get head.\n";
  return 0;
}

