#!/usr/bin/env bash

module purge

module load cmake
module load intel-gnu8-runtime/19.1.2.254
module load intelpython3 impi
module load boost

here=$(pwd $(dirname $0))
mkdir -p rel
pushd rel
cmake $here || exit -1
make || exit -1
popd
cp -v rel/dice ./


