#include <iostream>
#include <cstdlib>
#include <limits>
#include "boost/mpi.hpp"
#include <boost/iostreams/stream.hpp>

namespace mpi = boost::mpi;

std::ostream&
log(mpi::communicator const& world) {
  if (world.rank() == 0) {
    return std::cout;
  } else {
    namespace bios = boost::iostreams;
    typedef bios::stream<bios::null_sink> nullstream;
    static bios::null_sink null;
    static nullstream sink(null);
    return sink;
  }
}

void
print_placement(mpi::environment const& env, mpi::communicator const& world) {
  for(int rk = 0; rk < world.size(); ++rk) {
    world.barrier();
    if (rk == world.rank()) {
      std::cout << "Hi, this is processs " << world.rank() << " from node " << env.processor_name() << std::endl;
      std::cout << std::flush;
    }
  }  
}

void
play_dice(mpi::communicator const& world) {
  std::srand(world.rank()+world.size());
  int dice = (std::rand() % 6) + 1 ;
  log(world) << "Who's de best ?\n";
  int max = mpi::all_reduce(world, dice, mpi::maximum<int>());
  for(int rk = 0; rk < world.size(); ++rk) {
    world.barrier();
    if (rk == world.rank()) {
      std::cout << "This is " << world.rank() << ", I got " << dice << ", ";
      
      if (dice == max) {
        std::cout  << "I'm so talented\n";
      } else {
        std::cout  << "I'm such a looser...\n";
      }
    }
  }
}

int 
main(int argc, char* argv[]) {
  mpi::environment env;
  mpi::communicator world;
  print_placement(env, world);
  world.barrier();
  log(world) << "Let's play dices." << std::endl;
  world.barrier();
  play_dice(world);
  return 0;
}
