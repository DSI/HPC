This project provide documentation to the users of the computing facilities of the [Observatoire de la Côte d'Azur](https://www.oca.eu). That is, either
a user of the [licallo](https://crimson.oca.eu) HPC cluster, or a user of the [Lagrange](https://lagrange.oca.eu) clusters.

Please visit:
  * [Licallo specific documentation](/../wikis/Licallo) if you are a user of the HPC cluster. 
